
import sys, Queue, threading
from socket import *
from encryption import *

# Print Prompt: Function to write msg to screen
def prompt(username,msg) :
    sys.stdout.write('<' + username + '> ' + msg)
    sys.stdout.flush()

# Read STD IN: "Speak Function", reads a line from terminal and puts it in a queue so that
# the main thread can process for transmission
def speak(q,key):
    while True:
        msg = sys.stdin.readline()
        q.put(['s',encrypt(msg,key)])

# Read Socket: "Listen Function", reads data from  the wire and puts in it a queue so thatpython
# the main thread can process for display
def listen(q,s,key):
    while True:
        msg = s.recv(1024)
        q.put(['l',decrypt(msg,key)])
        

if __name__ == '__main__':

    mode = raw_input(" Choose operating mode: 'client' or 'server': ")
    username = raw_input (" Enter user name for this session: ")
    
    #client mode: sets the app in "caller" mode. the program will try to connect to remote host
    if mode == 'client':
        print (" client mode choose")
        serveraddress = raw_input(' Enter IPv4 Server Address: ')
        
        #Creating Socket and settings connecting
        s = socket(AF_INET,SOCK_STREAM)
        s.connect((serveraddress,8011))
        print(' Connected to ' + serveraddress)
        
        #Message Authentication Code/ HandShake / Negotiation
        #in this section we send a random msg to the remote host as ask for it back
        #if it matches our original msg we know we have the same key otherwise we must disconnect
        MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
        key = negotiate_key(s,'client')
        s.sendall(encrypt(MAC_LOCAL,key))
        MAC_HOST = s.recv(1024)
        MAC_HOST = decrypt(MAC_HOST,key)
        s.sendall(encrypt(MAC_HOST,key))
        MAC_RESPONSE = decrypt(s.recv(1024),key)
        if MAC_RESPONSE != MAC_LOCAL:
                  print(" BAD KEY NEGOTIATION... EXITING APPLICATION\n")
                  sys.exit()
        else:
                  print(" Negotiation Successful")
        
        s.sendall(username)
        usernameremote = s.recv(1024)
        print('\n ' + usernameremote + ' has joined the chat...\n')

        #Prepare Threads and Q
        q = Queue.Queue()

        speak_worker = threading.Thread(target=speak, args=(q,key))
        speak_worker.setDaemon(True)
        speak_worker.start()

        listen_worker = threading.Thread(target=listen, args=(q,s,key))
        listen_worker.setDaemon(True)
        listen_worker.start()

        # CHAT
        while True:
            if not q.empty():
                data = q.get()
                if data[0] == 'l':
                    if data[1] == ':exit:\n':
                        print(' Remote Connection Closed')
                        q.task_done()
                        sys.exit()

                    else:
                        prompt(usernameremote,data[1])
                        q.task_done()

                elif data[0] == 's':
                    if decrypt(data[1],key) == ':exit:\n':
                        print (' Exiting Application')
                        s.sendall(data[1])
                        q.task_done()
                        sys.exit()

                    else:
                        s.sendall(data[1])
                        q.task_done()
                    

    # server mode: sets the app in "receiver" mode. wait for connection from remote host
    elif mode == 'server':
        print (" server mode choosen")

        #Creating Socket and wait for connection from remote host
        print (' Wainting For Connection...')
        s = socket(AF_INET,SOCK_STREAM)
        s.bind(("",8011))
        s.listen(5)
        c,a = s.accept()
        print (" Received Connection From" + a[0])

        #Message Authentication Code/ HandShake / Negotiation
        MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
        key = negotiate_key(c)
        MAC_HOST = c.recv(1024)
        c.sendall(encrypt(MAC_LOCAL,key))
        MAC_HOST = decrypt(MAC_HOST,key)
        MAC_RESPONSE = decrypt(c.recv(1024),key)
        c.sendall(encrypt(MAC_HOST,key))
        
        if MAC_RESPONSE != MAC_LOCAL:
            print(" BAD KEY NEGOTIATION... EXITING APPLICATION\n")
            sys.exit()
        else:
            print(" Negotiation Successful")
            
            usernameremote = c.recv(1024)
            c.sendall(username)
            print('\n ' + usernameremote + ' has joined the chat...\n')
    
            #Prepare Threads and Q
            q = Queue.Queue()

            speak_worker = threading.Thread(target=speak, args=(q,key))
            speak_worker.setDaemon(True)
            speak_worker.start()

            listen_worker = threading.Thread(target=listen, args=(q,c,key))
            listen_worker.setDaemon(True)
            listen_worker.start()

        # CHAT
        while True:
            if not q.empty():
                data = q.get()
                if data[0] == 'l':
                    if data[1] == ':exit:\n':
                        print(' Remote Connection Closed')
                        q.task_done()
                        sys.exit()

                    else:
                        prompt(usernameremote,data[1])
                        q.task_done()

                elif data[0] == 's':
                    if decrypt(data[1],key) == ':exit:\n':
                        print (' Exiting Application')
                        c.sendall(data[1])
                        q.task_done()
                        sys.exit()

                    else:
                        c.sendall(data[1])
                        q.task_done()
    else:
        print ("Please choose valid mode: 'client' or 'server')")
        sys.exit()
