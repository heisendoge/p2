from Crypto import Random
import random
from Crypto.Cipher import *
import hashlib

random.seed()
#1024-bit MODP Group with 160-bit Prime Order Subgroup (not Sophie Germain Prime)
p = '0xB10B8F96'+'A080E01D'+'DE92DE5E'+'AE5D54EC'+'52C99FBC'+'FB06A3C6' + \
    '9A6A9DCA'+'52D23B61'+'6073E286'+'75A23D18'+'9838EF1E'+'2EE652C0' + \
    '13ECB4AE'+'A9061123'+'24975C3C'+'D49B83BF'+'ACCBDD7D'+'90C4BD70' + \
    '98488E9C'+'219A7372'+'4EFFD6FA'+'E5644738'+'FAA31A4F'+'F55BCCC0' + \
    'A151AF5F'+'0DC8B4BD'+'45BF37DF'+'365C1A65'+'E68CFDA7'+'6D4DA708' + \
    'DF1FB2BC'+'2E4A4371'

p = int(p,16)
       
g = '0xA4D1CBD5'+'C3FD3412'+'6765A442'+'EFB99905'+'F8104DD2'+'58AC507F' + \
    'D6406CFF'+'14266D31'+'266FEA1E'+'5C41564B'+'777E690F'+'5504F213' + \
    '160217B4'+'B01B886A'+'5E91547F'+'9E2749F4'+'D7FBD7D3'+'B9A92EE1' + \
    '909D0D22'+'63F80A76'+'A6A24C08'+'7A091F53'+'1DBF0A01'+'69B6A28A' + \
    'D662A4D1'+'8E73AFA3'+'2D779D59'+'18D08BC8'+'858F4DCE'+'F97C2A24' + \
    '855E6EEB'+'22B3B2E5'

g = int(g,16)

Q = 0xF518AA8781A8DF278ABA4E7D64B7CB9D49462353

#BS = Block_Size (we use pad and unpad functions to make sure the input to AES
#is always a multiple of 32bytes
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS) 
unpad = lambda s : s[0:-ord(s[-1])]

# modular exponentiation by squaring
# algorithm courtesy of
# http://eli.thegreenplace.net/2009/03/28/efficient-modular-exponentiation-algorithms
# usage: a to the power of b mod n
def modexp(a, b, n):
    r = 1
    while 1:
        if b % 2 == 1:
            r = r * a % n
        b /= 2
        if b == 0:
            break
        a = a * a % n

    return r

# This functions is used to negotiate a symmetric key (D-H Algorithm)
# Pre Condition: p= "Large Prime", g="Generator for the group Z* mod p", 
#				 h = "socket to negotiate connection from"
#				 mode = "App Current Operating Mode"
# functions return a sha256 32byte key to be used with AES
def negotiate_key(h,mode='server',p=p,g=g):
    
	# Generate a random exponent to use as secret
	exp = random.randrange(1,p)
	
	# if program in client mode, send the negotiation request
	if mode == 'client':
		h.sendall(bytes(modexp(g,exp,p)))
		seed = bytes(modexp(int(h.recv(1024)),exp,p))
		return hashlib.sha256(seed).digest()
	
	# if program in server mode, wait for negotiation request
	else:
		seed = modexp(int(h.recv(1024)),exp,p)
		h.sendall(bytes(modexp(g,exp,p)))
		return hashlib.sha256(bytes(seed)).digest()

# encryp raw msg and return a byte sting encryption    
def encrypt(msg,key):
    msg = pad(msg)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key,AES.MODE_CFB, iv)
    return iv + cipher.encrypt(bytes(msg))

# decrypt a bytestring and returns a raw msg 
def decrypt (enc,key):
    iv = enc[:16]
    enc = enc[16:]
    cipher = AES.new(key,AES.MODE_CFB,iv)
    return unpad(cipher.decrypt(enc))


# method to test key generation localy
def key_test(p=p,g=g):
	a = random.randrange(1,p)
	b = random.randrange(1,p)
	g_a = modexp(g,a,p)
	g_b = modexp(g,b,p)
	g_ab = modexp(g_a,b,p)
	g_ba = modexp(g_b,a,p)
	if g_ab == g_ba:
		return hashlib.sha256(bytes(g_ab)).digest()

