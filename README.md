# README #

Hi Team. 

I've changed the chat program code as well as the cryptography module code so that it will work with python 2.7.8. unfortunately to get a cross platform program going with 
python 3.4 (x64) and PyCrypto between windows/mac/linux is hell. with 2.7.8 (x86) works allot better.

one more thing. can we get a volunteer to start working on the attack program. Here are the things I think you need to know to accomplish this
    1 - set up your machine as a gateway
    2 - capture packages going from the server to the client
    3 - modify the packages and re-inject them in the other side of the network

    to do this you would have to figure out how to:
    1 - capture packages from the interface connected to the client (check out tcpdump, although I don't know how to capture the package out of the network, I only know how to sniff it using tcpdump/wireshark)
   2 - how to modify TCP/IP packages and re inject them in the other side of the interface
   I would like to get this done by next weekend that would be great. late me know what you find out. also ask the professor for help don't be shy.

this would be MiM hard more. if we can't do that way the professor said we can funnel the traffic to a python socket and deal with it that way.

### How do I get set up? ###

you will need python 2.7.8 for this project: https://www.python.org/downloads/
also PyCrypto. 
you can download the binaries for PyCrypto from:https://www.dlitz.net/software/pycrypto/
if you are using windows you can get the executable to install PyCrypto from: http://www.voidspace.org.uk/python/modules.shtml#pycrypto
in both cases we are using PyCrypto 2.6. I have no idea about macs.

### Contribution guidelines ###

we can discuss this later, but I figure forking and then requesting a pull is best. that way I can review the code and make sure it doesn't conflict with what we already have.
Also, make sure you put lots of comments on your code so we know whats happening in it.

### Who do I talk to? ###

* Michael da Cunha: michaelcunha57@gmail.com
* phraxos@phraxos.biz
* James Passaro: jimpassaro@gmail.com
* Leo Shum: light0315@gmail.com